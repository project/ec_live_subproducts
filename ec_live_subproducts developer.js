
/**
 * Format prices according reginal attributes
 *
 * Equivalent to E-Commerce (PHP) FormatPayment function
 * its faster than use ajax to format the number
 */
function FormatPrice(price, decimal, dpos, thousand, symbol, spos) {
  if (!dpos || !decimal) {return price;}
  price = Math.round(price * Math.pow(10, dpos)) / Math.pow(10, dpos);
  price = price + "";
  price = price.split(".");
  if (!price[0]) {price[0] = "0";}
  if (!price[1]) {price[1] = "";}
  if (price[1].length < dpos) {
    g = price[1];
    for (i = price[1].length + 1; i <= dpos; i++) {g += "0";}
    price[1] = g;
  }
  if (thousand != "" && price[0].length > 3) {
    h = price[0];
    price[0] = "";
    for(j = 3; j < h.length; j += 3) {
      i = h.slice(h.length - j, h.length - j + 3);
      price[0] = thousand + i + price[0] + "";
    }
    j = h.substr(0, (h.length % 3 == 0) ? 3 : (h.length % 3));
    price[0] = j + price[0];
  }
  decimal = (dpos <= 0) ? "" : decimal;
  if (spos == 1) {return symbol + price[0] + decimal + price[1];}
  else {return price[0] + decimal + price[1] + symbol;}
} // FormatPrice()

/**
 * Refresh the combination price
 *
 * Based on the attributes combination, refresh the
 * product price. If the combination if not possible
 * or their is no stock available, disable the "add to
 * cart" button
 */
function RefreshPrices() {
  var fcategory = new Array;
  var category = new Array;
  var price = Drupal.settings.price;
  var stock = -1;
  var forbid = false;
  for(variation in Drupal.settings.attr) {
    if (attr = Drupal.settings.attr[variation][Drupal.settings.attr[variation]["checked"]]) {
      $("#description-" + variation).html(attr["description"]);
      price += attr["price"];
      if (attr["fcategory"]) {fcategory = fcategory.concat(attr["fcategory"].split(","));}
      if (attr["category"]) {category = category.concat(attr["category"].split(","));}
      if ((stock > attr["stock"] && attr["stock"] != -1) || (stock == -1 && stock < attr["stock"]) ) {stock = attr["stock"];}
    }
  }
  // check if the fcategory category are present
  for (c in category) {
    for (f in fcategory) {
      if (category[c] == fcategory[f]) {
        stock = 0;
        forbid = true;
        break;
      }
    }
  }
  if (forbid) {$("#price > div").html(Drupal.settings.messages["fcategory"]);}
  else {
    if (stock != 0) {
      $("#price > div").html("<label>" + Drupal.settings.messages["price"] + ": </label>" +
        FormatPrice(price, Drupal.settings.FormatPrice["decimal"],
        Drupal.settings.FormatPrice["dpos"], Drupal.settings.FormatPrice["thousand"],
        Drupal.settings.FormatPrice["symbol"], Drupal.settings.FormatPrice["spos"]))
    }
    else {$("#price > div").html(Drupal.settings.messages["sold_out"]);}
  }
  $("#edit-add-to-cart").attr("disabled", !stock);
} // RefreshPrices()

/**
 * Initialize all variables.
 */
$(function() {
  $("#edit-priceupdate").css("display", "none");
  $(".attr").click(function() {
    if (this.checked || this.type == "select-one") {
      Drupal.settings.attr[this.name.replace(/variations\[/, "").replace(/\]/, "")]["checked"] = this.value;
      RefreshPrices();
    }
  });
  RefreshPrices();
});