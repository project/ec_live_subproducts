********************************************************************************
                     D R U P A L    M O D U L E
********************************************************************************
Module Name        : EC Live Subproducts
Original Author    : Bruno Massa http://drupal.org/user/67164
Project Page       : http://drupal.org/project/ec_live_subproducts
Support Queue      : http://drupal.org/project/issues/ec_live_subproducts

********************************************************************************
DESCRIPTION
Imagine you want to sell shirts with diferent sizes and colors. With normal products module, included in ecommerce package, with should create one by one.
With EC Live Subproducts, you create first each variation (size and color) and the module creates for you all possible combinations! And for your customers, its also easy to choose theirs product combination.


FEATURES
* Dynamic Price: each attribute can have a price surcharge
* Dynamic Stock: if a single attribute runs out of stock, all combinations are blocked
* Default Selection: you might choose what attributes will be selected by default
* Description: show the attribute and variation description and images
* SKU creation: Automatic SKU combination
* Forbid any undesired combination: not allow create a soft drink with Cola flavor and Green color
* Limitless combinations: more than 100, 200k, 500k, 500M, 500B! its up to your database
* Runs smoothly: minimum server overload
* JavaScript: enhance the module presentation, but its not required
* Customize: use CSS to give a new look to your product page
* XHTML Strict 1.0, CSS 2 compliant, PHP E_ALL and Drupal code standard compliant


INSTALATION
1 * Place the entire directory into your Drupal /modules/ directory.
2 * Enable the module by going to: administer > modules (Drupal 4.7) or administer > site building > modules (Drupal 5) and selecting ec_live_subproducts.
3 * EC_Live_Subproducts requires a special product type (like EC Tangible Subproduct, included with this module) : administer > modules > ec_tangible_subproduct
4 * Create variations and attributes on administer > store > product variations
5 * Its done!


DEVELOPMENT
Do you has a doubt, a new idea or a complain about this module? Let me know! Im gonna be glad to answer.
Do you program on PHP and want to help? Its gonna be a pleasure have some help. Send you code now!
Do you translated to a new language? I will include it on the next release!


TODO
* Attributes surcharge reflecting the actual options on-the-fly
* Customized "This-Drupal-Site Recommends"
* Non-tangible subproduct module (like services)
* Massive operations like changing EVERY attributes prices at once
* Something like an API to let other modules interact
* Deeper documentation

FAQ
##Q Whats the differrence between this module and the original subproducts module, that comes with ecommerce module?
  * Dynamic stock and price calculation
  * Variation and attributes description
  * Attributes SKU
  * JavaScript and other eye candy features
  * The combinations are not created at once: new products (node) are created on each cron, when they are sold and when you click on a "create" link. This avoid huuuuuge servers overload when creating very complex combinations.

##Q Why not contribute with the original subproducts module instead create a new one?
I am contributing! As long the E-Commerce maintainers should keep the code safest as possible, i decided to publish the new working "beta" code as a separated module. The final objective is to merge with the original subproducts module.

##Q Who is using this module comercially?
Titan Atlas, a Brazillian hardware and software company.
They are also the module sponsors.